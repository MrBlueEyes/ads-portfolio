import glob
from collections import Counter
from stopwords import allStopWords
import string

all_files = glob.glob('Gutenberg SF/*.*')
def file_contents(file_name):
    f = open(file_name)
    try:
        return f.read()
    finally:
        f.close()
datasource = dict((file_name, file_contents(file_name)) for file_name in all_files)

results = Counter()

def get_word_count(content):
    words = []
    for w in v.split():
        word = w.translate(None, string.punctuation).lower()

        if len(word) > 1 and word not in allStopWords:
            words.append(word)

    return Counter(words)

for k,v in datasource.items():
    result = get_word_count(v)
    for x,y in result.items():
        results[x] += y

results = sorted(results.items(), key=lambda x: x[1], reverse=True)

for x in results[:10]:
    print x[0] + ' ' + str(x[1])