#!/usr/bin/env python
import mincemeat 
import glob
from stopwords import allStopWords
all_files = glob.glob('Gutenberg/Gutenberg Small/*.*')def file_contents(file_name):    f = open(file_name)    try:        return f.read()    finally:        f.close()data = dict((file_name, file_contents(file_name)) for file_name in all_files)

def mapfn(k, v):
    for w in v.split():
        yield w, 1

def reducefn(k, vs):
    result = 0
    for v in vs:
        result += v
    return result

s = mincemeat.Server()

# The data source can be any dictionary-like object
s.datasource = data
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="changeme")

#sort results from high to lowresults = sorted(results.items(), key=lambda x: x[1], reverse=True)#print results
print(results[:10])
i = open('outfile','w')i.write(str(results))i.close()
