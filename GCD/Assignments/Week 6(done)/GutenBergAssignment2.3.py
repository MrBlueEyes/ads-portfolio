import glob
import re
import operator
import stopwords
import time

current_milli_time = lambda: int(round(time.time() * 1000))

startTime = current_milli_time()

print('Trying to acces the map')
all_files = glob.glob('Gutenberg/Gutenberg SF/*.*')

print('Creating function to read the files')
def file_contents(file_name):
    f = open(file_name, encoding="charmap")
    try:
        return f.read()
    except UnicodeEncodeError:
        f.close()
    finally:
        f.close()
        
print('Getting the datasource')
datasource = dict((file_name, file_contents(file_name)) for file_name in all_files)

lists = []
allWords = []
for file_name in all_files:
    print('Getting the text for : ' + file_name)
    text = datasource[file_name]
    #print(text)
    
    print('Removing all the special characters')
    textWithoutSpecial = re.sub('[^a-zA-Z0-9-_ ]', '', text)
    #print(textWithoutSpecial)
    
    print('Getting all words, also avoiding all words with less the 2 characters')
    words = [x.lower() for x in textWithoutSpecial.split(' ') if len(x) > 1 and x.lower() not in stopwords.allStopWords]
    #print(words)
    
    print('Getting all the unique words')
    allWords.extend(list(set(words)))
    uniqueWords = dict((word, 0) for word in list(set(words)))
    #print(uniqueWords)  
    
    print('Counting the words')
    for word in words:
        uniqueWords[word] = uniqueWords[word] + 1
        #print(uniqueWords[word])

    print('Done counting')
    lists.append(uniqueWords)
    
print('Done: got ' + str(len(lists)) + ' lists')    

print('Getting all the unique words')
allUniqueWords = dict((word, 0) for word in allWords)
#print(allUniqueWords)

print('Counting the words')
#TOO MANY VALUES TO UNPACK
for currentList in lists:
    #print('Got a list')

    for thisWord in currentList:
        thisValue = currentList[thisWord]
        allUniqueWords[thisWord] = allUniqueWords[thisWord] + thisValue
        
print('Sort')
sorted_x = sorted(allUniqueWords.items(), key=operator.itemgetter(1))
sorted_x.reverse()
print('List sorted')

index = 1
for item in sorted_x[:10]:
    print(index, item)
    index = index + 1
    
stopTime = current_milli_time()

print(startTime)
print(stopTime)
print('It took:')
print(stopTime - startTime)
print('Ms:')