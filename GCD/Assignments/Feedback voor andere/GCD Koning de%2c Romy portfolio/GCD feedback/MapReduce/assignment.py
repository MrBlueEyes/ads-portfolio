#!/usr/bin/env python
import mincemeat 
import glob
from stopwords import allStopWords
from collections import Counter
import re

all_files = glob.glob('Gutenberg/Gutenberg Small/*.*')
def file_contents(file_name):
    f = open(file_name)
    try:
        return f.read()
    finally:
        f.close()
data = dict((file_name, file_contents(file_name)) for file_name in all_files)

words = [];

for name,co in data.items():
    for word in co.split():
        finalWord = re.sub(r'[^\w\s]','',word).lower()
    
        if len(finalWord) > 1 and finalWord not in allStopWords:
            words.append(word)

results = Counter(words);

#sort results from high to low
results = sorted(results.items(), key=lambda x: x[1], reverse=True)
#print results
print(results[:10])

i = open('outfile','w')
i.write(str(results))
i.close()
